using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MissionMenu : MonoBehaviour
{
    public GameObject player;
    public GameObject spaceship;
    public GameObject menu;
    public bool isMissionActive = false;
    public int missionCount = 0;

    void Update()
    {
        if (isMissionActive)
        {
            print("Mision activa siuuuuuuu");
            QuestInProcess();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Equals("Station"))
        {
            print("Entra a la estaci�n");
            menu.SetActive(true);

            //Time.timeScale = 0;
            //UnityEngine.XR.InputTracking.disablePositionalTracking = false;

        }
    } 

    private void OnTriggerExit(Collider other)
    {
        if (other.name.Equals("Station"))
        {
            print("Sale de la estaci�n");
            menu.SetActive(false);
            //Time.timeScale = 1;
        }
    }

    public void AcceptMission()
    {
        if (isMissionActive == false)
        {
            print("Mission Accepted");
            isMissionActive = true;
            missionCount = UnityEngine.Random.Range(1, 6);
            print("Destroy " + missionCount + " enemy spaceships!");

        }
        else
        {
            print("You already have a mission active!");
        }
    }

    public void QuestInProcess()
    {
        //pasan cosas y la missionCount--

        if (missionCount == 0) 
            AccomplishMission();
    }

    public void AccomplishMission()
    {
        print("Mission Accomplished");
        isMissionActive = false;
    }

    public void RepairSpaceship()
    {
        //spaceship.RestoreHealth()
        if (player.GetComponent<PlayerStats>().money > 0) 
        {
            player.GetComponent<PlayerStats>().health = 3;
            player.GetComponent<PlayerStats>().money--;
            print("Spaceship Health Restored, now your money is: "+ player.GetComponent<PlayerStats>().money);
        }else {
            print("Not enough money to restore the spaceship health");
        }
        
    }

    public void ExitMenu()
    {
        menu.SetActive(false);
        print("Exiting Menu");
        //Time.timeScale = 1;
    }

}
