using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class RadarController : MonoBehaviour
{
    GameObject shipDecoy;
    GameObject radar;
    GameObject radarZone;
    //[SerializeField] private List<EnemyController> m_Enemies = new();
    GameObject[] previousEnemyDots;
    public HashSet<EnemyController> m_Enemies = new HashSet<EnemyController>();
    public GameObject prefabEnemyRadar;
    [SerializeField] LayerMask mask;

    public void Start()
    {

        shipDecoy = GameObject.FindGameObjectWithTag("DecoyShip");
        radar = GameObject.FindGameObjectWithTag("Radar");
        radarZone = GameObject.FindGameObjectWithTag("RadarZone");

        StartCoroutine("RadarScan");
    }

    IEnumerator RadarScan()
    {
        while(true) {

            Collider[] tmp = Physics.OverlapSphere(radarZone.transform.position, radarZone.transform.lossyScale.x, mask);

            m_Enemies.Clear();
            print(m_Enemies.Count);

            foreach (Collider c in tmp)
            {
                EnemyController ec = c.attachedRigidbody.transform.GetComponent<EnemyController>();
                if (ec != null)
                {
                    m_Enemies.Add(ec);
                }
            }


            previousEnemyDots = GameObject.FindGameObjectsWithTag("EnemyDot");

            foreach (GameObject dot in previousEnemyDots) {
                Destroy(dot);
            }

            Vector3 globalPositionPlayer = shipDecoy.transform.position;

            foreach (EnemyController enemy in m_Enemies)
            {
                Vector3 globalPosition = enemy.transform.position;
                Vector3 direction = (globalPosition - globalPositionPlayer).normalized;

                float a = Mathf.InverseLerp(0f, radarZone.transform.localScale.x, Vector3.Distance(transform.position, globalPosition));
                float b = Mathf.Lerp(0f, radar.transform.lossyScale.x, a);

                GameObject enemyDot = Instantiate(prefabEnemyRadar);
                enemyDot.transform.position = radar.transform.position + direction * b;
                enemyDot.transform.SetParent(radar.transform);
                enemyDot.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
                enemyDot.transform.localEulerAngles = new Vector3(0.04f * Time.time, 0.04f * Time.time, 0.04f * -Time.time);
                Debug.DrawLine(globalPositionPlayer, globalPosition + (direction * 10), Color.red, 1f);
            }

            yield return new WaitForSeconds(0.2f);
        }
    }

    private void OnDrawGizmos()
    {
        if (radarZone != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(radarZone.transform.position, radarZone.transform.localScale.x);
        }
    }
}
