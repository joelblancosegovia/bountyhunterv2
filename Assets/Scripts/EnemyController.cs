using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private GameEventEnemy m_OnEnemySpawn;
    [SerializeField] private GameEventEnemy m_OnEnemyDies;

    private void Start()
    {
        m_OnEnemySpawn.Raise(this);
    }

    private void OnDestroy()
    {
        m_OnEnemyDies.Raise(this);
    }
}
