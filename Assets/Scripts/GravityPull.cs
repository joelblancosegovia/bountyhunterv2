using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityPull : MonoBehaviour
{

    GameObject ship;
    GameObject gravityPull = null;
    readonly float G = 125000f;

    void Start()
    {
        ship = GameObject.FindGameObjectWithTag("DecoyShip");
    }

    // Update is called once per frame
    void Update()
    {
        applyGravity();
    }


    private void applyGravity()
    {
        if (gravityPull != null)
        {
            float m2 = gravityPull.GetComponent<Rigidbody>().mass;
            float r = Vector3.Distance(ship.transform.position, gravityPull.transform.position);

            ship.GetComponent<Rigidbody>().AddForce((gravityPull.transform.position - ship.transform.position).normalized * (G * (ship.GetComponent<Rigidbody>().mass * m2) / (r * r)));
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (gravityPull == null)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("GravitationalPull"))
            {
                gravityPull = other.gameObject.transform.parent.gameObject;
                Debug.Log(gravityPull.name);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (gravityPull != null)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("GravitationalPull"))
            {
                gravityPull = null;
                Debug.Log("Saliendo del pozo de gravedad");
            }
        }
    }


}
