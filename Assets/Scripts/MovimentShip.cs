using Autohand;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using Unity.XR.OpenVR;
using Unity.XR.Oculus;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;


public class MovimentShip : MonoBehaviour
{

    PhysicsGadgetJoystick joystick;
    PhysicsGadgetHingeAngleReader throttle;
    HingeJoint throttleJoint;
    JointSpring jointSpring;
    GameObject shipController;
    GameObject shipDecoy;
    GameObject objectThrottle;
    GameObject falseThrottle;
    GameObject[] heavyBlasters;
    GameObject[] blasters;
    public GameObject prefabBullet;
    GameObject player;
    private GameObject trackers;
    private GameObject playerPosition;
    Vector2 joystickForce;
    float leverPosition;

    float bulletLifetime = 5f;
    int gunTurn;
    Boolean canShoot = false;
    Boolean bulletReady = false;


    

    // Start is called before the first frame update
    void Start()
    {
        joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<PhysicsGadgetJoystick>();
        throttle = GameObject.FindGameObjectWithTag("Thrust").GetComponent<PhysicsGadgetHingeAngleReader>();
        objectThrottle = GameObject.FindGameObjectWithTag("Thrust");
        falseThrottle = GameObject.FindGameObjectWithTag("FalseThrust");
        throttleJoint = GameObject.FindGameObjectWithTag("Thrust").GetComponent<HingeJoint>();
        jointSpring = throttleJoint.spring;
        shipController = GameObject.FindGameObjectWithTag("Ship");
        shipDecoy = GameObject.FindGameObjectWithTag("DecoyShip");
        heavyBlasters = GameObject.FindGameObjectsWithTag("HeavyBlaster");
        blasters = GameObject.FindGameObjectsWithTag("Blaster");
        /*chair = GameObject.FindGameObjectWithTag("Seat");
        player = GameObject.FindGameObjectWithTag("Player");
        trackers = GameObject.FindGameObjectWithTag("Trackers");
        playerPosition = GameObject.FindGameObjectWithTag("PlayerFixedPosition");
        playerContainer = GameObject.FindGameObjectWithTag("PlayerContainer");*/

        gunTurn = 1;

        StartCoroutine(checkShootDelay());

    }

    // Update is called once per frame
    void Update()
    {
        obtainDirection();
        obtainSpeed();
        //fixPlayer();
    }

    private void fixPlayer()
    {
        player.transform.position = playerPosition.transform.position;
    }

    public void LockThrottle()
    {
        jointSpring.damper = 1000000;
    }

    public void FreeThrottle()
    {
        jointSpring.damper = 10;
    }

    public void HoldingJoystick()
    {
        canShoot = true;
    }

    public void FreeJoystick()
    {
        canShoot = false;
    }

    void obtainDirection()
    {
        joystickForce = joystick.GetValue();
        shipDecoy.transform.Rotate(new Vector3(joystickForce.y * 5, 0, -joystickForce.x * 5));
    }

    void obtainSpeed()
    {
        //falseThrottle.transform = throttle.transform;
        leverPosition = throttle.GetValue();
        shipDecoy.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * (leverPosition / 10));
    }



    //Obtener la se�al de que pulsan el boton de disparo en el mando de las Oculus (Trigger) entonces que esta lance la funcion "Shoot"

    private void Shoot()
    {

        if (bulletReady && canShoot)
        {
            Vector3 bulletSpawnPosition = new Vector3();


            switch (gunTurn)
            {
                case 1:
                    gunTurn = 2;
                    bulletSpawnPosition = heavyBlasters[0].transform.position;
                    break;
                case 2:
                    bulletSpawnPosition = heavyBlasters[1].transform.position;
                    gunTurn = 1;
                    break;
            }

            bulletSpawnPosition.z += 10;

            GameObject bullet = Instantiate(prefabBullet) as GameObject;
            bullet.transform.position = bulletSpawnPosition;
            bullet.GetComponent<Rigidbody>().AddForce(shipDecoy.transform.forward * 5);
            bullet.transform.rotation = shipDecoy.transform.rotation;
            Destroy(bullet, bulletLifetime);
            bulletReady = false;
        }
    }

    IEnumerator checkShootDelay()
    {
        while (true)
        {
            bulletReady = true;
            yield return new WaitForSeconds(0.1f);
        }
    }


}
